## Gestione Manifestazioni  
  
Software to manage non competitive RC modeling events written by GAMBAS for GNU/Linux  
Full control of radio frequency, list of pilots, reservation flight, statistics, cronology  
More panels for visualization of pilots situations.  
Gestione manifestazioni require Gambas language
For installation and use of Gambas pleare refere to
https://gambas.sourceforge.net/en/main.html
For information and help about Gestione Manifestazioni
akrobaticone@gmail.com


Software per la gestione di eventi modellistici non competitivi scritto in GAMBAS su OS GNU/LINUX
Controllo frequenze radio,lista piloti con prenotazione voli, statistiche, cronologia.
Pannelli per la visualizzazione stato piloti e situazione voli.
Per utilizzare Gestione manifestazioni è necessario installare il linguaggio Gambas 
seguendo le istruzioni alla pagina https://gambas.sourceforge.net/en/main.html
Per qualsiasi richiesta o approfondimento
akrobaticone@gmail.com

