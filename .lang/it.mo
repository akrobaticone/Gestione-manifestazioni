��    �     4  �  L      h(  �F  i(      o     ,o     ;o     Go     \o  	   po     zo     �o     �o     �o     �o  
   �o     �o  
   �o     �o     �o     p     p     p     'p     0p     3p     <p     ?p     Ap     Ip     Mp     ep     ~p     �p     �p     �p     �p     �p     �p     �p     q     q     *q     7q     Hq     Xq     pq     �q     �q     �q     �q     �q     �q      �q  #   r     5r     Ar     Nr  N   Vr  @   �r  ;   �r  W   "s     zs  3   �s     �s  5   �s  <   t  l   Tt  "   �t     �t     �t     �t     �t  5   	u     ?u     Fu     ]u     du     uu     �u     �u     �u     �u     �u     �u     �u     �u     �u     v     v     $v  	   :v     Dv     Uv     gv     }v  !   �v     �v     �v     �v     �v      �v     w     w     w     3w  	   Aw     Kw     Tw  4   jw     �w     �w     �w     �w     �w     �w     	x     x  
   "x     -x     >x  
   Qx     \x     kx     ox     �x  %   �x     �x     �x     �x     �x     �x     �x     
y      y     5y     By     Gy     Oy     Xy     hy     py     �y     �y     �y  	   �y     �y     �y     �y     �y     �y     �y     �y      �y     z  #   z     ?z     Yz  5   bz  .   �z     �z     �z  	   �z     �z     {     {  	   {     %{     >{     T{     X{     g{     p{  U   �{     �{     �{     |  !   .|     P|     g|     o|     v|     �|     �|     �|  
   �|     �|     �|     �|     �|     }     }     *}     2}     D}     W}  
   _}     j}     ~}  '   �}     �}     �}     �}     �}     ~     ~     8~     R~     h~     }~     �~     �~     �~     �~     �~        
     
   "     -     6  i   ?  Z   �     �  E   �     R�     Y�     l�     {�     ��     ��     ��     ��     ɀ     ۀ     ��     �     �     &�     9�     T�     i�     n�     ��     ��     ��     ρ     �  	   �     ��     ��     �     �     �     (�     9�     I�     Y�     l�  
   ��     ��     ��     ��     ��     ��     Ƃ  +   ς     ��     �     �     �     �  
   *�     5�     F�     S�     a�     m�     |�     ��     ��     ��     ��     ��     ��     Ӄ     ��  [   ��      [�     |�     ��     ��     ��     ��     ʄ     ΄     ׄ     �     �     �     �     �     $�     8�     I�     Z�     u�     ��     ��     ��     Ņ     Յ     �     ��     �  
   /�  ;   :�     v�     �     ��  A   ��     І     ׆     ��  *   �  A   6�     x�     ��     ��     ��     ��     ��     Ї     �     �     �     �     0�     =�     K�  
   X�     c�      �     ��     ��  P   ň  9   �  �   P�  	   �  !   �     4�     =�     N�     ^�  	   s�     }�     ��     ��     ��     ۊ     ��     �  	   1�     ;�     A�     D�     L�     Q�     l�     r�     ��  
   ��  /   ��     Ջ  
   �     ��     �     (�     8�     N�  ?   d�     ��     ��     Ɍ     �     �     ��  '   	�  +   1�     ]�     t�     ��     ��     ��  	   ��     ��     ��  	   ύ  
   ٍ     �     �     ��     �     �     �     *�     :�     A�     M�     S�     `�     n�  �   �     Y�  	   g�     q�     ��     ��     ��     ��     ��     Ï  (   ӏ  "   ��     �     -�  ^   A�     ��     ��     ܐ     �  
   �     �     �     %�     =�     B�     U�     e�     {�     ��     ��     ��     ��     ˑ     ��     �     ��     �      �     $�  "   2�     U�     h�     |�  &   ��  	   ��  
   ��     ˒     ݒ  %   ��     �      �     '�     .�     6�     F�     ]�  !   l�     ��     ��     ��     ��     ē  8   ˓     �     �  
   "�     -�     ;�     C�     F�     V�  	   f�  	   p�     z�  �   ��  �F  ~�      �     <�     I�     U�     j�  	   ~�     ��     ��     ��     ��     ��  
   ��     ��  
   ��      �     �     �     �     "�     3�     <�     ?�     H�     K�     M�     U�     Y�     q�     ��     ��     ��     ��     ��     ��     ��     �     �     !�     6�     C�     T�     d�     |�     ��     ��     ��     ��     ��     ��      ��  #   �     A�     N�     [�  N   c�  @   ��  ;   ��  W   /�     ��  3   ��     ��  4   ��  <   #�  l   `�  "   ��     ��     ��     ��     �  5   �     K�     R�     i�     p�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     !�     0�  	   F�     P�     a�     s�     ��  !   ��     ��     ��     ��     ��      ��     �     "�     +�     ?�  	   M�     W�     `�  4   v�     ��     ��     ��     ��     ��     �     �     "�  
   .�     9�     J�  
   ]�     h�     w�     {�     ��  %   ��     ��     ��     ��     ��     ��     �     �     ,�     A�     N�     S�     [�     d�     t�     |�     ��     ��     ��  	   ��     ��     ��     ��     ��     ��     ��     ��      ��     �  #   '�     K�     e�  5   n�  .   ��     ��     ��  	   ��     	�     �     �  	   &�     0�     I�     _�     c�     r�     {�  U   ��     ��     �     �     9�     Y�     p�     x�     �     ��     ��     ��  
   ��     ��     ��     ��     ��     
�     %�     2�     :�     L�     _�  
   g�     r�     ��  '   ��     ��     ��     ��     ��     �     #�     @�     Z�     p�     ��     ��     ��     ��     ��     ��     �  
   �  
   *�     5�     >�  i   G�  Z   ��     �  E   �     Z�     a�     t�     ��     ��     ��     ��     ��     ��     ��      �     �     !�     .�     A�     \�     q�     v�     ��     ��     ��     ��     ��  	   ��     ��     �     �     �     '�     0�     A�     Q�     a�     t�  
   ��     ��     ��     ��     ��     ��     ��  +   ��     �     �     �     !�     &�  
   2�     =�     N�     [�     i�     u�     ��     ��     ��     ��     ��     ��     ��     ��     ��  [   �      `�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     !�     )�     =�     N�     `�     {�     ��     ��     ��     ��     ��     ��     �     �  	   3�  ;   =�     y�     ��     ��  A   ��     ��     ��     ��  *   �  A   9�     {�     ��     ��     ��     ��     ��     ��     ��     �     �     !�     3�     @�     N�  
   [�     f�      ��     ��     ��  P   ��  9   �  �   S�  	   �  !   �     7�     @�     P�     `�  	   u�     �     ��     ��     ��     ��     ��     �  	   3�     =�     C�     F�     N�     S�     n�     t�     ��  
   ��  /   ��     ��  
   ��     �     �     *�     :�     P�  ?   f�     ��     ��     ��     ��     ��     ��  '   �  +   3�     _�     v�     ��     ��     ��  	   ��     ��     ��  	   ��  
   ��     ��     ��     ��     �     �     �     -�     =�     D�     P�     V�     c�     q�  �   ��     \�  	   j�     t�     ��     ��     ��     ��     ��     ��  (   ��  "   ��     !�     /�  ^   C�     ��     ��     ��     ��  
   �     �     �     '�     ?�     D�     W�     g�     }�     ��     ��     ��     ��     ��     ��     ��      �     �     "�     &�  "   4�     W�     j�     ~�  &   ��  
   ��  
   ��     ��     ��  %   ��          #     )     0     8     H     _  !   n     �     �     �     �     �  7   �          
   #    .    <    D    G    W 	   g 	   q    {    )       ?   �  B       `           �      f   �   o   �  �  �  �   �  �       �       �  �   �   �   �  �  ~   �  "   �   \  �   i       $   H   �           �   d   L   2  �   �  �          �   �  �   �  <      �   S   �                    �     *      �       7             �     �  �   3   6     n  =   �  c  m   u  �    �  O                (   ,  |   ~  C   5   �    q   �       y  #   -   b    �               	  Z  �   c   X   }      �      �                �   h    �    �   �  �   ,   �  �       '  �  �  �  M  k      ^   b      9       �       (  �      ;         �   �  W  �     p              �   �          �      �   �       �   
  M   �   �  9  �   �   �  Y      �      �  �     1   �      �   �  �  [   u   �  �  �      �  �  `  a  D          �   j          �   1     n           �   �   -         Q       �      �  �   A      �       �   �       �   �              q  G       &       .   N   �   �               �  j           >  �  s                      �   �   ]      �       �  !   8       �  �   Y   �          �   F       @  �      �       �   �  %              �  �   �  Z   P  �   \          �  �   �   �   "             �   �          �   t                      �  x  *   �     �   �           G              K  �      �  e              �  �  �   5  +           �  �   �              �      T  �  
   3               �  E      8  �  '   U   �  �           :   �  U  ;         v   �  �     �  _   :  �       %       w  !      �      �   {       4  �   �  �   �      N  R           #      v  �         l  �  �   �  J   �  �   w           6     �    �       E   o                     d      �   �   z  L  r    V  A  e  �  �   �  B  C      ]   �   �       |  f  �       =      z                   �  �       ?  y   l          �  x   �  �   �   �  �   �   W   g   �   F  �   g  R  +     }       �   h   �  {  �   4   �       �   �     �   .  �         �      �   V   �   S  �   �              i  �  �   _  2   �       k           p   I   �      t   �  D   X  �   P           �       �  �   �   �      /       �   �  7  O             �   <   �       a          /          �   I  �  �     �       &  �  r   0      s       $  @   �   �   0   �  )  �       Q  ^  �              �   �              	   J  �   [  �  �  K          �   �   �   �  m  �   T      �      �   >       �  H       GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

                    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

                            NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


Also add information on how to contact you by electronic and paper mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, the commands you use may
be called something other than `show w' and `show c'; they could even be
mouse-clicks or menu items--whatever suits your program.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the program, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the program
  `Gnomovision' (which makes passes at compilers) written by James Hacker.

  <signature of Ty Coon>, 1 April 1989
  Ty Coon, President of Vice

This General Public License does not permit incorporating your program into
proprietary programs.  If your program is a subroutine library, you may
consider it more useful to permit linking proprietary applications with the
library.  If this is what you want to do, use the GNU Library General
Public License instead of this License.
     Riepilogo frequenze in uso :     Versione:    Nazione:    Riepilogo generale  - Crea nuovo Timer  01:10:10  File audio   File cronologia   Lista gestione frequenze   No  Opzioni  Pilota n   Salvataggio configurazione   caratteri  con  (  con (  già in volo!  ok  ** Eventi da :  *******  00 12:30:00 99 ? ALLARME AUT Abilita suoneria eventi Abilita trasmissione FTP Aggiungi Aggiungi
evento Aggiungi evento cronologia Aggiungi modello Aggiungi parametro statistico Aggiungi suoni All'evento: Annulla Annulla Chiamata Annulla prenotazione Annulla volo Annulla_chiamata Annullare volo? Annullata chiamata volo Annullata prenotazione volo Annullato Round  Annullato volo Applica Applicazione Apri cartella programma Apri form prenotazioni Apri il file dopo la generazione Aprire il file dopo la generazione? Ara testo 2 Area Testo 1 Aspetto Attenzione superato il numero massimo di piloti ammessi. Continuare lo stesso? Attenzione,raggiunto numero massimo di voli ammesso, continuare? Attenzione: Nome pilota già presente,aggiungere lo stesso? Attenzione: Per visualizzare queste informazioni fare doppio click sul nome del pilota  Attenzione: Pilota  Attenzione: Pilota gia presente con lo stesso round Attenzione: file non conforme! Attenzione: frequenza già presente nel gruppo Round  Attenzione: frequenza non disponibile, aggiungere lo stesso? Attenzione: i contatori vengono salvati insieme alla lista piloti e non nelle impostazioni dell'applicazione Attenzione:frequenza già presente Attivi Attivo Avvia Avvio cronologia Avvisa se pilota gia  presente 
in lista prenotazioni Avvisi Avvisi e comunicazioni Azioni Azzera contatori Azzera cronologia CHE CHIAMATA Cambia
modello Cambia Modello Cambia cartella Cambia modello Cambiato modello con:  Cancella Cancella cronologia Cancella pilota Cancella tutto Cancellata cronologia Carattere Carattere avvisi Carattere tabelle Carica configurazione Carica lista piloti Cartella predefinita applicazione Chiamata
volo Chiamata al volo Chiamata volo Chiudi Chiusura Gestione Manifestazioni Collaboratori Commento Conta alla rovescia Conta fino a: Contatori Continua Continua il conteggio Continuando si cancella la lista attuale, procedere? Conto alla rovescia da: Copia Corpo del commento Costruttore Costruttore coincide con pilota Costruttore: Costruttore= Costruttori Crea Timer Crea nuovo Timer Crea nuovo modello Cronologia Cronologia.pdf DEU Dati da inizializzare Disabilita trasmissione FTP ERRORE FTP: attivo ma non configurato ESP Edita Elimina Elimina round Elimina suono Eliminare il pilota  Eliminazione da lista Emetti avviso sonoro Errore FTP:  Esci Esporta Esporta  Eventi generici Eventi- Evento
Cronologia Evento cronologia Evento generico Evento pilota Evidenzia Evidenziazione FINE FRA FTP Ferma File File CSV File configurazione non conforme File di testo File già esistente, sovrascrivere? File non audio o corrotto File pdf File salvataggio corrotto
Si avvia una nuova sessione File salvato con diversa versione di programma Filtra piloti evidenziati Fine tutti i voli Fine volo Fine volo pilota Font Freq.=  Frequenza Frequenza già chiamata! Funzione disabilitata GBR Genera elenchi Generico Gestione Manifestazioni Gestione Manifestazioni:  Software written by Gambas on GNU/LInux O.S. - 2020  M.V. - Gestione avvisi su tabellone Gestione manifestazioni Gestione manifestazioni    Gestione manifestazioni - Informa GestioneManifestazioni Globale Gruppo Gruppo
Round Gruppo modellistico Hanno collaborato: ID INVISIBILE ITA Immagini/Radio.jpg Imposta ora allarme: Imposta tempo da raggiungere: Imposta tempo di partenza: Impostazioni In volo Inchiostro avvisi Inchiostro tabelle Includi Informa su Informazioni Pilota Informazioni di sistema Informazioni su Gestione Manifestazioni Inglese Inizializza Inizializza piloti Inizializzare tutti i piloti? Inizializzazione piloti Inserire nazionalità pilota Inserire nome costruttore Inserire nome modello Inserire nome pilota Inserisci
pilota Inserisci Pilota Inserisci nome contatore Inserisci nuovo pilota Inserisci pilota Inserisci titolo manifestazione Intervallo invio (sec) Invisibile Invisibili Iscritto Italiano L'ultima sessione non si è chiusa regolarmente

File Salvataggio non trovato
Si avvia una nuova sessione L'ultima sessione non si è chiusa regolarmente

caricare l'ultima situazione memorizzata? Licenza Liimite massimo di voli permessi per pilota,
se 0 nessuna limitazione Lingua ListBox Inchiostro ListBox sfondo Lista Costruttori Lista Modelli Lista Piloti Lista Prenotazioni:  Lista Round Lista costruttori Lista frequenze radio in uso Lista modelli Lista nazionalità Lista piloti Lista prenotazioni Manda in volo gruppo Round Mantieni dati piloti Max  Media modelli per costruttore Media modelli per pilota Media voli per pilota Messaggi di sistema Messaggio all'allarme Minuti Modalità Modelli Modello Modello attuale Modello= Modifica Modifica 
pilota Modifica Pilota Modifica pilota Modifica priorità Modifica priorità prenotazione Modificato N massimo voli N. Voli NO Nazionalità Nazione Nazione: Nessuna applicazione associata ai file .csv Nessuna limitazione Niente No Nome Nome Pilota Nome Timer Nome costruttore Nome modello Nome modello: Nome pilota Non comunicata Non salvare Nota Note Note aggiuntive Numero Numero Piloti iscritti Numero costruttori Numero di righe visualizzate Numero di voli Numero massimo di piloti ammisssibile in volo contemporaneamente. Se 0 nessuna limitazione. Numero massimo di piloti in volo Numero modelli volanti Numero piloti Numero voli effettuati Nuovo Nuovo Timer N° N° voli N° voli effettuati OK Ok Opzione disabilitata Opzione non abilitata Opzioni Ora di attivazione: Ora inizio volo: Ora prenotazione Ora stimata ultimo volo:   Ora stimata utimo volo Ora ultimo volo Ora ultimo volo=  Ordina lista volo per: Ordina per nome Ordina per numero Ordina per numero voli Ordina per ora ultimo volo Ordina per tempo volo Pagina n.  Parametri aggiuntivi da visualizzare in tabella statistiche Parziale Password Pausa Per rendere effettiva la scelta riavviare Gestione manifestazioni Pilota Pilota con tempo volo piu alto: Pilota già in volo Pilota già in volo, aggiungere lo stesso? Pilota già presente in lista prenotazioni, aggiungere lo stesso? Pilota: Pilota= Piloti Piloti con più voli: Piloti evidenziati Piloti in stato attivo: Piloti in stato invisibile: Piloti in stato sospeso: Predefiniti Prenota volo Prenotazione volo Prenotazioni Prenotazioni- Prima pagina Principale Priorità prima posizione ( Priorità scambio con pilota N*  Priorità ultima posizione ( Procedi Programma per la gestione delle frequenze radio per manifestazioni modellistiche Programma per la gestione di manifestazioni modellistiche Questa funzione permette di salvare periodicamente la situazione piloti permettendone così il ricupero in caso di blocco del sistema.Se impostata a 0 la funzione risulta disabilitata Revisioni Riassegna numeri piloti eliminati Riattiva Riattiva 
Piloti Riattiva Piloti Riavvio applicazione Riepilogo Riepilogo situazione Piloti Riepilogo totale Rimuovi dalla lista Rimuovi parametro statistico Ripristina piloti eliminati Ripristinato stato attivo Ripristino dopo arresto anomalo Rotazione Round SI SOSPESO Sali Sali in lista prenotazione Salva Salva Configurazione Salva lista piloti Salva ogni Salvare lista attuale prima di creare la nuova? Salvare lista prima di uscire? Salvataggi Salvataggio automatico Salvataggio lista Scambia con N° Scambiata Priorità ( Scambiata priorità ( Scegliendo SI verrà cancellata la sessione attuale, procedere? Scelta Suoneria Scelta modello per:  Scende in lista prenotazione Scendi Scrivi
Avvisi Scrivi Avvisi Sei sicuro di cancellare la cronologia? Sei sicuro di inizializzare tutti i piloti? Seleziona stato piloti Sfondo avvisi Sfondo tabelle Si Si  Sicurezza Software scritto utilizzando: Solo messaggio Solo nomi Solo stato Solo suoneria Sospesi Sospeso Sottotitolo Sovrascrivi Sposta al primo Sposta in fondo Stampa Statistiche Stato Stato attivo Stato attuale Stato invisibile Stato invisibile: stessa condizione di eliminato ma ripristinabile
Stato sospeso: non visualizzato in lista ma presente per tutte altre opzioni
per modificare questi due stati utilizzare menu Piloti->Riattiva Piloti
  Stato sospeso Strumenti Suoneria + messaggio Suoneria eventi Suoneria pulsante Suoni Sì Tabellone Totale Tabellone stato Tempo che intercorre tra due voli (min)
 Tempo di pausa 
tra due voli (min) Tempo di volo Tempo di volo (min) Tempo di volo assegnato ad ogni pilota. Se diverso da 0 verrà emesso un avviso quando scaduto Tempo medio di volo per pilota Tempo rotazione pagine (Sec) Tempo totale di volo Tempo trascorso: Tempo volo Tempo_di_volo=  Termina Terminare tutti i voli? Test Timer senza evento Tipo di evento: Tipo inizializzazione Titolo Titolo Manifestazione Tot. tempo voli Totale Totale Nazioni partecipanti Totale tempo di volo Totale tempo volo Totale voli Trasmissione FTP effettuata Turno URL Ultimi eventi Uscire da Gestione Manifestazioni? Vai al primo posto Vai in ultimo posto Verifica frequenze Verifica in corso, attendere.........
 Versione: Visualizza Visualizza orario Visualizza prenotazioni Visualizzazione Form riepilogo totale Voli Voli=  Volume Volume  Volume suoneria akrobaticone@gmail.com attivo all'ora cambiato nome manifestazione in:  commento conta in avanti elimina eliminare contatore  gruppo il cambiamento della lingua avverrà al prossimo riavvio impostato Round  in volo con  interrompi lista_modelli modello no non competitive nuovo conteggio priorità solo nomi statistiche.pdf Project-Id-Version: Gestione-manifestazioni 3.18.3
PO-Revision-Date: 2023-09-09 11:16 UTC
Last-Translator: Michele <akrobaticone@gmail.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
     GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

                    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

                            NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


Also add information on how to contact you by electronic and paper mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, the commands you use may
be called something other than `show w' and `show c'; they could even be
mouse-clicks or menu items--whatever suits your program.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the program, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the program
  `Gnomovision' (which makes passes at compilers) written by James Hacker.

  <signature of Ty Coon>, 1 April 1989
  Ty Coon, President of Vice

This General Public License does not permit incorporating your program into
proprietary programs.  If your program is a subroutine library, you may
consider it more useful to permit linking proprietary applications with the
library.  If this is what you want to do, use the GNU Library General
Public License instead of this License.
 Riepilogo frequenze in uso:   Versione:    Nazione:    Riepilogo generale  - Crea nuovo Timer  01:10:10  File audio  File cronologia  Lista gestione frequenze   No  Opzioni  Pilota n   Salvataggio configurazione   caratteri  con  (  con (  già in volo!  ok ** Eventi da :   *******  00 12:30:00 99 ? ALLARME AUT Abilita suoneria eventi Abilita trasmissione FTP Aggiungi Aggiungi
evento Aggiungi evento cronologia Aggiungi modello Aggiungi parametro statistico Aggiungi suoni All'evento: Annulla Annulla Chiamata Annulla prenotazione Annulla volo Annulla_chiamata Annullare volo? Annullata chiamata volo Annullata prenotazione volo Annullato Round  Annullato volo Applica Applicazione Apri cartella programma Apri form prenotazioni Apri il file dopo la generazione Aprire il file dopo la generazione? Area testo 2 Area testo 1 Aspetto Attenzione superato il numero massimo di piloti ammessi. Continuare lo stesso? Attenzione,raggiunto numero massimo di voli ammesso, continuare? Attenzione: Nome pilota già presente,aggiungere lo stesso? Attenzione: Per visualizzare queste informazioni fare doppio click sul nome del pilota  Attenzione: Pilota  Attenzione: Pilota gia presente con lo stesso round Attenzione: file non conforme! Attenzione: frequenza già presente nel gruppo Round Attenzione: frequenza non disponibile, aggiungere lo stesso? Attenzione: i contatori vengono salvati insieme alla lista piloti e non nelle impostazioni dell'applicazione Attenzione:frequenza già presente Attivi Attivo Avvia Avvio cronologia Avvisa se pilota gia  presente 
in lista prenotazioni Avvisi Avvisi e comunicazioni Azioni Azzera contatori Azzera cronologia CHE CHIAMATA Cambia
modello Cambia modello Cambia cartella Cambia modello Cambiato modello con:  Cancella Cancella cronologia Cancella pilota Cancella tutto Cancellata cronologia Carattere Carattere avvisi Carattere tabelle Carica configurazione Carica lista piloti Cartella predefinita applicazione Chiamata
volo Chiamata al volo Chiamata
volo Chiudi Chiusura Gestione Manifestazioni Collaboratori Commento Conta alla rovescia Conta fino a: Contatori Continua Continua il conteggio Continuando si cancella la lista attuale, procedere? Conto alla rovescia da: Copia Corpo del commento Costruttore Costruttore coincide con pilota Costruttore: Costruttore= Costruttori Crea Timer Crea nuovo Timer Crea nuovo modello Cronologia Cronologia.pdf DEU Dati da inizializzare Disabilita trasmissione FTP ERRORE FTP: attivo ma non configurato ESP Edita Elimina Elimina Round Elimina suono Eliminare il pilota  Eliminazione da lista Emetti avviso sonoro Errore FTP:  Esci Esporta Esporta  Eventi generici Eventi- Evento
Cronologia Evento cronologia Evento generico Evento pilota Evidenzia Evidenziazione FINE FRA FTP Ferma File File CSV File configurazione non conforme File di testo File già esistente, sovrascrivere? File non audio o corrotto File pdf File salvataggio corrotto
Si avvia una nuova sessione File salvato con diversa versione di programma Filtra piloti evidenziati Fine tutti i voli Fine volo Fine volo pilota Font Freq.= Frequenza Frequenza già chiamata! Funzione disabilitata GBR Genera elenchi Generico Gestione Manifestazioni Gestione Manifestazioni:  Software written by Gambas on GNU/LInux O.S. - 2020  M.V. - Gestione avvisi su tabellone Gestione Manifestazioni Gestione manifestazioni    Gestione Manifestazioni-Informa GestioneManifestazioni Globale Gruppo Gruppo
Round Gruppo modellistico Hanno collaborato: ID INVISIBILE ITA Immagini/Radio.jpg Imposta ora allarme Imposta tempo da raggiungere: Imposta tempo di partenza: Impostazioni In volo Inchiostro avvisi Inchiostro tabelle Includi Informa su Informazioni Pilota Informazioni di sistema Informazioni su Gestione Manifestazioni English Inizializza Inizializza piloti Inizializzare tutti i piloti? Inizializzazione piloti Inserire nazionalità pilota Inserire nome costruttore Inserire nome modello Inserire nome pilota Inserisci
pilota Inserisci Pilota Inserisci nome contatore Inserisci nuovo pilota Inserisci pilota Inserisci titolo manifestazione Intervallo invio (sec) Invisibile Invisibili Iscritto Italiano L'ultima sessione non si è chiusa regolarmente

File Salvataggio non trovato
Si avvia una nuova sessione L'ultima sessione non si è chiusa regolarmente

caricare l'ultima situazione memorizzata? Licenza Liimite massimo di voli permessi per pilota,
se 0 nessuna limitazione Lingua ListBox Inchiostro ListBox sfondo Lista Costruttori Lista Modelli Lista Piloti Lista Prenotazioni:  Lista Round Lista costruttori Lista frequenze radio in uso Lista modelli Lista nazionalità Lista piloti Lista prenotazioni Manda in volo gruppo Round Mantieni dati piloti Max  Media modelli per costruttore Media modelli per pilota Media voli per pilota Messaggi di sistema Messaggio all'allarme Minuti Modalità Modelli Modello Modello attuale Modello= Modifica Modifica 
pilota Modifica Pilota Modifica pilota Modifica priorità Modifica priorità prenotazione Modificato N massimo voli N. Voli NO Nazionalità Nazione Nazione: Nessuna applicazione associata ai file .csv Nessuna limitazione Niente No Nome Nome Pilota Nome Timer Nome costruttore Nome modello Nome modello: Nome pilota Non comunicata Non salvare Nota Note Note aggiuntive Numero Numero Piloti iscritti Numero costruttori Numero righe visualizzate Numero di voli Numero massimo di piloti ammisssibile in volo contemporaneamente. Se 0 nessuna limitazione. Numero massimo di piloti in volo Numero modelli volanti Numero piloti Numero voli effettuati Nuovo Nuovo Timer N° N° voli N° voli effettuati OK Ok Opzione disabilitata Opzione non abilitata Opzioni Ora di attivazione: Ora inizio volo: Ora  prenotazione Ora stimata ultimo volo:   Ora stimata utimo volo Ora ultimo volo Ora ultimo volo= Ordina lista volo per Ordina per nome Ordina per numero Ordina per numero voli Ordina per ora ultimo volo Ordina per tempo volo Pagina n. Parametri aggiuntivi da visualizzare in tabella statistiche Parziale Password Pausa Per rendere effettiva la scelta riavviare Gestione manifestazioni Pilota Pilota con tempo volo piu alto: Pilota già in volo Pilota già in volo, aggiungere lo stesso? Pilota già presente in lista prenotazioni, aggiungere lo stesso? Pilota: Pilota= Piloti Piloti con più voli: Piloti evidenziati Piloti in stato attivo: Piloti in stato invisibile: Piloti in stato sospeso: Predefiniti Prenota volo Prenotazione volo Prenotazioni Prenotazioni- Prima pagina Principale Priorità prima posizione ( Priorità scambio con pilota N*  Priorità ultima posizione ( Procedi Programma per la gestione delle frequenze radio per manifestazioni modellistiche Programma per la gestione di manifestazioni modellistiche Questa funzione permette di salvare periodicamente la situazione piloti permettendone così il ricupero in caso di blocco del sistema.Se impostata a 0 la funzione risulta disabilitata Revisioni Riassegna numeri piloti eliminati Riattiva Riattiva
Piloti Riattiva Piloti Riavvio applicazione Riepilogo Riepilogo situazione Piloti Riepilogo totale Rimuovi dalla lista Rimuovi parametro statistico Ripristina piloti eliminati Ripristinato stato attivo Ripristino dopo arresto anomalo Rotazione Round Si SOSPESO Sali Sali in lista prenotazione Salva Salva Configurazione Salva lista piloti Salva ogni Salvare lista attuale prima di creare la nuova? Salvare lista prima di uscire? Salvataggi Salvataggio automatico Salvataggio lista Scambia con N° Scambiata Priorità ( Scambiata priorità ( Scegliendo SI verrà cancellata la sessione attuale, procedere? Scelta suoneria Scelta modello per:  Scende in lista prenotazione Scendi Scrivi
Avvisi Scrivi avvisi Sei sicuro di cancellare la cronologia? Sei sicuro di inizializzare tutti i piloti? Seleziona stato piloti Sfondo avvisi Sfondo tabelle Si Si  Sicurezza Programma scritto utilizzando: Solo messaggio Solo nomi Solo stato Solo suoneria Sospesi Sospeso Sottotitolo Sovrascrivi Sposta al primo Sposta in fondo Stampa Statistiche Stato Stato attivo Stato attuale Stato invisibile Stato invisibile: stessa condizione di eliminato ma ripristinabile
Stato sospeso: non visualizzato in lista ma presente per tutte altre opzioni
per modificare questi due stati utilizzare menu Piloti->Riattiva Piloti
  Stato sospeso Strumenti Suoneria + messaggio Suoneria eventi Suoneria pulsante Suoni Si Tabellone Totale Tabellone Stato Tempo che intercorre tra due voli (min)
 Tempo di pausa 
tra due voli (min) Tempo di volo Tempo di volo (min) Tempo di volo assegnato ad ogni pilota. Se diverso da 0 verrà emesso un avviso quando scaduto Tempo medio di volo per pilota Tempo rotazione pagine (sec) Tempo totale di volo Tempo trascorso: Tempo Volo Tempo_di_volo=  Termina Terminare tutti i voli? Test Timer senza evento Tipo di evento: Tipo inizializzazione Titolo Titolo manifestazione Tot. tempo voli Totale Totale nazioni partecipanti Totale tempo di volo Totale tempo volo Totale voli Trasmissione FTP effettuata Turno URL Ultimi eventi Uscire da Gestione Manifestazioni? Vai al primo posto Vai in ultimo posto Verifica frequenze Verifica in corso, attendere.........
 Versione:  Visualizza Visualizza orario Visualizza prenotazioni Visualizzazione Form riepilogo totale Voli Voli= Volume Volume  Volume suoneria akrobaticone@gmail.com attivo all'ora Cambiato nome manifestazione in:  commento conta in avanti elimina eliminare contatore  gruppo Il cambamento della lingua avverrà al prossimo riavvio Impostato Round  in volo con  Interrompi lista_modelli modello no Non competitive nuovo conteggio priorità solo nomi statistiche.pdf 