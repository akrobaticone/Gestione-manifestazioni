### Installation
Gestione Manifestazioni is written in Gambas language
Gambas is a visual object oriented language similar to Microsoft Visual Basic but is NOT a clone
In order to use it open a terminal and type the following commands  
sudo add-apt-repository ppa:gambas-team/gambas3  
sudo apt-get update  
sudo apt-get install  gambas3  
In this way last version of Gambas is installed on your system
Please send me a report in case of problems or bugs  
Any suggestions to improve the program or to add new features are welcome  
Thank you and enjoy Gestione Manifestazioni  
  
Michele akrobaticone@gmail.com